# CHANGELOG

## 0.9.7
- Fix opening-hours-py package version in setup.py and requirements.txt files

## 0.9.6
- Fix continuous integration bug (building images for children projects) with creating a new tag

## 0.9.5
- Refactore custom checks by creating two new mother classes according the custom check is related to one or many columns
- Add new custom check `one-of-required` to deal with siret or rna numbers 

## 0.9.4
- Fix compatibility bug to be able to install validata-core with pip
- Add new functionality to ignore custom check on empty values
- Fix broken link in documentation

## 0.9.3

- Fix compatibility bug with python versions anterior to 3.9
- Fix update requirements.txt file for validata core children projects (so far validata-api and validata-ui) in CI when an update occurs on validata-core project

## 0.9.2

- Fix packages version number in cascade deployments for validata core children projects (so far validata-api and validata-ui)

## 0.9.1

- Fix authentification issue in cascade deployments for validata core children projects (so far validata-api and validata-ui)

## 0.9.0

- Allows to ignore header-case sensitivity (cli, code, and api)
- Solve abnormal count errors in stat report when the custom check is unknown
- Automatically update validata-core version in requirements for validata-core children projects (so far validata-api and validata-ui)

## 0.8.6

- Fix `french_gps_coordinates` custom check.
- Update frictionless package from 4.18.2 to 4.38.0. This leads, among other, to the possible usegit status of custom checks

## 0.8.5

- Add new `french_gps_coordinates` custom check.

## 0.8.4

- Each custom check now gets a unique code
- Custom check now emits `check-error` errors during `validate-start` method
- Unknown custom check in a schema emits a `check-error`

## 0.8.3

- Really fix previous xlsx issue (for uploaded file AND url resource)

## 0.8.2

- Fix [xlsx issue](https://gitlab.com/validata-table/validata-ui/-/issues/99)

## 0.8.1

- Add new unit test
- Adapt error messages

## 0.8.0

- Update CI python version to 3.9
- Update CI to install `rustc` and `cargo` debian packages to install `opening-hours` extension
- Add new `phone-number` custom check
- Upgrade to last version of frictionless-py (`4.18.2`)
- Split unit tests on custom checks into separate test files
- Improve error messages for `nomenclature-actes` custom check
- Add new `opening-hours` custom check

## 0.7.6

- Raise ValidataSourceException before frictionless crash
- Make unit tests pass

## 0.7.5

- Code refactoring
- Fix 'skip required header' check when custom checks are defined

## 0.7.4

- Upgrade requirements (freeze frictionless dependency to latest `4.10.6`)

## 0.7.3

- Add `ValidataSourceError` to handle with exceptions raised during source reading
- Extend test coverage
- Fix cli when a source contains an encoding problem

## 0.7.2

- Handle text files with [BOM](https://en.wikipedia.org/wiki/Byte_order_mark)

## 0.7.1

- Install `frictionless[excel]` to handle .xls files (freeze frictionless dependency to `4.2.1`)

## 0.7.0

- Merge `towards_frictionless_4` branch into master

## 0.7.0a7

- Adapt error filters based on tags
- Remove unused import
- Fix flake8 issues

## 0.7.0a6

- Fix enum error message

## 0.7.0a5

- Update to frictionless 4.2.1

## 0.7.0a4

- Update to frictionless 4.2.0
- Handle array items relative errors

## 0.7.0a3

- Update to frictionless 4.1.0

## 0.7.0a2

- Fix crash while computing badge info
- Formatting

## 0.7.0a1

- Update to frictionless 4.\*
  - report property 'tables' renamed 'tasks'
  - report property 'table' renamed 'resource'
  - custom check classes:
    - remove `prepare` method and put content in constructor
    - rename `validate_task` method into `validate_start`
    - access to table data is now done via `self.resource`

## 0.6.0

- Update README.md
- Fix linter issue

## 0.6.0a9

- Turn structure warnings into call to actions
- Normalize error messages (start with an uppercase letter and end with a period)

## 0.6.0a8

- fix badge metrics computing if no table errors

## 0.6.0a7

- fix packaging issues

## 0.6.0a6

- fix flake issue
- add is_body_error and is_structure_error functions

## 0.6.0a5

- improve dev configuration
- remove dead code
- fix flake8 issues
- use black

## 0.6.0a4

- Add encoding unit tests
- Change `_detect_encoding` method into `detect_encoding` class method
- Fix extract data encoding

## 0.6.0a3

- Fix double header bug

## 0.6.0a2

- still fixing error messages
- adjustments to last frictionless release

## 0.6.0a1

- Upgrade frictionless dependency to 3.48.0
- Embed common code to handle source data (used by validata-ui and validata-api)
- Move error messages into error_messages.py
- Fix tests

## 0.6.0a0

- Migrate from goodtables-py to frictionless-py

## 0.5.7

- Freeze requirements
- Unlock tableschema version

## 0.5.6

- Reorder imports
- Harden custom checks against invalid input
- Fix error messages
- Set tableschema module version

## 0.5.5

- Don't validate empty values
- Update goodtables module version

## 0.5.4

- Fix 'duplicate-header' crash

## 0.5.3

- Add 'extra-header' translation

## 0.5.2

- Add 'lxml' forgotten dependency

## 0.5.1

- Add 'ezodf' forgotten dependency

## 0.5.0

- Handle supernumerary columns:
  - allow to 'repair' tabular file before content validation

## 0.4.2

- Add `french_siren_value` custom check (thx to Antoine Augusti)

## 0.4.0

Breaking changes:

- Replace `validata_code.Validator` class by `validata_code.validate` function.

## 0.3.5

Non-breaking changes:

- Define new error: `unknown-csv-dialect`

## 0.3.3

Non-breaking changes:

- Import custom checks statically

## 0.3.1, 0.3.2

Non-breaking changes:

- Add metadata to `setup.py`

## 0.3.0

Breaking changes:

- Remove anything related to [SCDL](http://scdl.opendatafrance.net/), or make it configurable.

## 0.2.1

Non-breaking changes:

- Implement `validata_core.compute_badge` function.

## 0.2.0

Breaking changes:

- Add `validata_code.Validator` class.
- Remove `validata_code.validate` function.

## 0.1.1

Non-breaking changes:

- Add `improve_messages` function enhancing messages as required by the Validata project. Call it from `validata_core.validate`.
- Some of the errors of the report returned by `validata_core.validate` now contains 2 new properties (`title`, `content`).
